package models

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"go-gin-example/pkg/util"
	service "go-gin-example/service/casbin_service"
	"strconv"
	"strings"
	"time"
)

var order string

type Manager struct {
	MgId      int        `gorm:"primary_key" json:"id"`
	RoleName  string     `json:"role_name" gorm:"default:''"`
	RoleId    string     `json:"-" gorm:"default:'40'"`
	MgName    string     `json:"username"`
	MgPwd     string     `json:"-"`
	CreatedAt time.Time  `json:"create_time"`
	MgMobile  string     `json:"mobile"`
	MgEmail   string     `json:"email"`
	MgState   int        `json:"mg_state" gorm:"default:0"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-" gorm:"default:''"`
}

//interface{}通过断言转换成[]interface{}类型
func InterfaceToSliceInterface(a interface{}) []interface{} {
	b, _ := a.([]interface{}) //通过断言实现类型转换
	return b
}

func removePro(ddbenv []interface{}, k int) []interface{} {
	return append(ddbenv[:k], ddbenv[k+1:]...)
}

//[]interface{}类型中是否存在某字符串 并删除字符串返回新的[]interface{}
func IsExistInArray(value string, array []interface{}) []interface{} {
	for k, v := range array {
		if v == value {
			order = "mg_id " + value
			newarray := removePro(array, k)
			return newarray
		}
	}
	return array
}

// 添加用户
// AddArticle add a single article
func AddManager(data map[string]interface{}) (*Manager, error) {
	manager := Manager{
		MgName:   data["mg_name"].(string),
		MgPwd:    data["mg_pwd"].(string),
		MgEmail:  data["mg_email"].(string),
		MgMobile: data["mg_mobile"].(string),
		RoleId:   "40",
		MgState:  0,
	}
	err := db.Create(&manager).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	errs := db.First(&manager, manager.MgId).Error
	if errs != nil && errs != gorm.ErrRecordNotFound {
		return nil, errs
	}
	return &manager, nil
}

// 获取记录总数
func GetManagerTotal(maps map[string]interface{}) (int, error) {
	cond := maps["whereSQL"]
	vals := IsExistInArray("asc", InterfaceToSliceInterface(maps["vals"]))
	zvals := IsExistInArray("desc", vals)

	var count int
	if err := db.Model(&Manager{}).Where(cond, zvals...).Count(&count).Error; err != nil {
		return 0, err
	}
	return count, nil
}

// 根据用户名判断用户是否存在
func ExistManagerByName(name string) (bool, error) {
	var manager Manager
	err := db.Select("mg_id").Where("mg_name = ?", name).First(&manager).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}

	if manager.MgId > 0 {
		return true, nil
	}

	return false, nil
}

//根据用户名返回用户所拥有的权限节点列表
func GetManagerRolesIdByName(name string) []string {
	fmt.Println("打印传过来的用户名")
	fmt.Println(name)
	var manager Manager
	var role Role
	err := db.Select("role_id").Where("mg_name = ?", name).First(&manager).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		fmt.Println("查询用户名出现错误")
	}
	RoleIdsList := strings.Split(manager.RoleId, ",")
	fmt.Println("打印该用户拥有的角色")
	fmt.Println(RoleIdsList)
	var PsIdsList []string
	PsIdsListStr := ""
	for _, v := range RoleIdsList {
		RoleId, _ := strconv.Atoi(v)
		fmt.Println("打印该用户遍历的RoleId")
		fmt.Println(RoleId)
		errs := db.Select("ps_ids").Where("role_id = ?", RoleId).First(&role).Error
		if errs != nil && errs != gorm.ErrRecordNotFound {
			fmt.Println("查询用户名出现错误")
		}
		PsIdsListStr = PsIdsListStr + "," + role.PsIds
	}
	PsIdsList = strings.Split(PsIdsListStr, ",")
	//权限去重
	PsIdsList = util.RemoveReplicaSliceString(PsIdsList[1:])
	return PsIdsList
}

// 根据用户id判断用户是否存在
func ExistManagerByID(id int) (bool, error) {
	var manager Manager
	err := db.Select("mg_id").Where("mg_id = ?", id).First(&manager).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}
	if manager.MgId > 0 {
		return true, nil
	}
	return false, nil
}

// 根据用户id修改用户
func EditManager(id int, data interface{}) (*Manager, error) {
	var Managers Manager
	err := db.Debug().Model(&Managers).Where("mg_id = ?", id).Updates(data).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	errs := db.Where("mg_id = ?", id).First(&Managers).Error
	if errs != nil && errs != gorm.ErrRecordNotFound {
		return nil, errs
	}
	return &Managers, nil
}

// 根据用户id分配用户角色
func SetRoleManager(id int, data interface{}) (*Manager, error) {
	var Managers Manager
	e := service.Enforcer
	err := db.Debug().Model(&Managers).Where("mg_id = ?", id).Updates(data).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	errs := db.Where("mg_id = ?", id).First(&Managers).Error
	if errs != nil && errs != gorm.ErrRecordNotFound {
		return nil, errs
	}

	e.DeleteUser(Managers.MgName)
	UserRoleIdList := strings.Split(Managers.RoleId, ",")
	for _, v := range UserRoleIdList {
		IntV, _ := strconv.Atoi(v)
		PolicyRoleName := getRoleNameString(IntV)
		//添加用户对应的角色关系
		e.AddRoleForUser(Managers.MgName, PolicyRoleName)
	}
	e.LoadPolicy()

	return &Managers, nil
}

// 根据id获取单条数据
func GetManagerSingle(id int) (*Manager, error) {
	var manager Manager
	err := db.Where("mg_id = ?", id).First(&manager).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	return &manager, nil
}

// 根据pageNum页码，pageSize每页条数 综合查询条件 获取所有记录
func GetManager(pageNum int, pageSize int, maps map[string]interface{}) ([]*Manager, error) {
	cond := maps["whereSQL"]
	vals := IsExistInArray("asc", InterfaceToSliceInterface(maps["vals"]))
	zvals := IsExistInArray("desc", vals)
	var Managers []*Manager
	err := db.Where(cond, zvals...).Offset((pageNum - 1) * pageSize).Limit(pageSize).Order(order).Find(&Managers).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}

	for _, v := range Managers {
		var roleRoleName Role
		sRoleName := roleRoleName.getRoleName(v.RoleId)
		if len(sRoleName) == 0 {
			v.RoleName = "超级管理员"
		} else {
			v.RoleName = strings.Replace(strings.Trim(fmt.Sprint(sRoleName), "[]"), " ", ",", -1)
		}

	}
	return Managers, nil
}

// 根据id删除用户
func DeleteManger(id int) error {
	if err := db.Where("mg_id = ?", id).Delete(&Manager{}).Error; err != nil {
		return err
	}
	return nil
}

//token授权
func CheckAuth(username, password string) string {
	var auth Manager
	db.Select("mg_id").Where(Manager{MgName: username, MgPwd: password}).Where("mg_state != 0").First(&auth)
	if auth.MgId > 0 {
		return util.EncodeMD5(string(auth.MgId) + string(auth.MgEmail))
	}
	return "no"
}
