package models

import (
	"fmt"
	"github.com/jinzhu/gorm"
)

type PermissionApi struct {
	PsId       int              `gorm:"primary_key" json:"id"`
	Name       string           `json:"authName"`
	PsApiPath  string           `json:"path"`
	Children   []*PermissionApi `json:"children"`
	PsApiOrder int              `json:"order"`
}

//根据传过来的id获取指定的PermissionApi数据
func (p *PermissionApi) getPermissionApi(id int) (ret []*PermissionApi) {
	if err := db.Where("ps_id = ?", id).First(&ret).Error; err != nil {
		fmt.Println(err)
	}
	return
}


// GetTags gets a list of tags based on paging and constraints
func GetPermissions(pageNum int, pageSize int, maps interface{}) ([]*PermissionApi, error) {
	var permissionapis []*PermissionApi

	err := db.Debug().Where("`ps_api_service` is null").Where("`ps_api_action` is null").Where("`ps_api_order` is not null").Offset(pageNum).Limit(pageSize).Order("ps_api_order ASC").Find(&permissionapis).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	for _, v := range permissionapis {
		//根据pid获取名称
		var childrenPsName Permission
		cRes := childrenPsName.getPermission(v.PsId)

		var childrenPsIds []*Permission
		if err := db.Select("ps_id, ps_level").Where("ps_pid = ?", v.PsId).Where("ps_level = '1'").Find(&childrenPsIds).Error; err != nil {
			return nil, err
		}
		v.Name = cRes[0].PsName
		//fmt.Println("打印permission表ids")
		//fmt.Println(childrenPsIds)
		myIds := make([]int, 0)
		for _, vid := range childrenPsIds {
			//fmt.Println(vid.PsId)
			myIds = append(myIds, vid.PsId)
		}
		var results []*PermissionApi
		if err := db.Where("`ps_id` in (?)", myIds).Find(&results).Error; err != nil {
			return nil, err
		}
		for _, vz := range results {
			var childrenPsName Permission
			cRes := childrenPsName.getPermission(vz.PsId)
			vz.Name = cRes[0].PsName
		}

		v.Children = results
		//fmt.Println(myIds)
	}
	return permissionapis, nil
}

// GetArticles gets a list of articles based on paging constraints
//func GetPermissions(pageNum int, pageSize int, maps interface{}) ([]*Permission, error) {
//	var Permissions []*Permission
//	err := db.Preload("Tag").Where(maps).Offset(pageNum).Limit(pageSize).Order("id DESC").Find(&Permissions).Error
//	if err != nil && err != gorm.ErrRecordNotFound {
//		return nil, err
//	}
//
//	return articles, nil
//}

// GetArticle Get a single article based on ID
//func GetArticle(id int) (*Article, error) {
//	var article Article
//	err := db.Where("id = ?", id).First(&article).Error
//	if err != nil && err != gorm.ErrRecordNotFound {
//		return nil, err
//	}
//
//	err = db.Model(&article).Related(&article.Tag).Error
//	if err != nil && err != gorm.ErrRecordNotFound {
//		return nil, err
//	}
//
//	return &article, nil
//}

// EditArticle modify a single article
//func EditArticle(id int, data interface{}) error {
//	if err := db.Model(&Article{}).Where("id = ?", id).Updates(data).Error; err != nil {
//		return err
//	}
//
//	return nil
//}

//// AddArticle add a single article
//func AddArticle(data map[string]interface{}) error {
//	article := Article{
//		TagID:         data["tag_id"].(int),
//		Title:         data["title"].(string),
//		Desc:          data["desc"].(string),
//		Content:       data["content"].(string),
//		CreatedBy:     data["created_by"].(string),
//		State:         data["state"].(int),
//		CoverImageUrl: data["cover_image_url"].(string),
//	}
//	if err := db.Create(&article).Error; err != nil {
//		return err
//	}
//
//	return nil
//}
//
//// DeleteArticle delete a single article
//func DeleteArticle(id int) error {
//	if err := db.Where("id = ?", id).Delete(Article{}).Error; err != nil {
//		return err
//	}
//	return nil
//}
//
//// CleanAllArticle clear all article
//func CleanAllArticle() error {
//	if err := db.Unscoped().Where("deleted_at != ? ", 0).Delete(&Article{}).Error; err != nil {
//		return err
//	}
//
//	return nil
//}
