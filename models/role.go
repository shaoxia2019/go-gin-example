package models

import (
	"fmt"
	"github.com/jinzhu/gorm"
	service "go-gin-example/service/casbin_service"
	"strconv"
	"strings"
)

type Role struct {
	RoleId   int        `json:"id" gorm:"primary_key"`
	PsIds    string     `json:"-"`
	RoleName string     `json:"roleName"`
	RoleDesc string     `json:"roleDesc"`
	Children []*TreeOne `json:"children,omitempty"`
}

//根据role_id获取角色名称返回[]string
func (r *Role) getRoleName(rid string) (ret []string) {
	ridSlice := strings.Split(rid, ",")
	var RoleInfo []*Role
	var RoleName []string
	err := db.Where("role_id in (?)", ridSlice).Find(&RoleInfo).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		fmt.Println("查询出错")
	}
	for _, v := range RoleInfo {
		RoleName = append(RoleName, v.RoleName)
	}
	return RoleName
}

//根据role_id获取角色名称string
func getRoleNameString(rid int) string {
	var RoleInfo Role
	err := db.Where("role_id = ?", rid).First(&RoleInfo).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		fmt.Println("查询出错")
	}
	return RoleInfo.RoleName
}

// 根据用户id判断角色是否存在
func ExistRoleNewByID(id int) (bool, error) {
	var role Role
	err := db.Select("role_id").Where("role_id = ?", id).First(&role).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}
	if role.RoleId > 0 {
		return true, nil
	}
	return false, nil
}

// 根据用户id判断角色是否存在 以及角色下的权限是否存在
func ExistRoleByID(id int, rid int) (bool, error) {
	var role Role
	db.Debug().Raw("SELECT role_id, ps_ids FROM `sp_role` WHERE role_id = ? and find_in_set(?, ps_ids)", id, rid).Scan(&role)
	if role.RoleId > 0 {
		return true, nil
	}
	return false, nil
}

func removeList(strlist []string) []string {
	for k, v := range strlist {
		if v == "0" {
			strlist = append(strlist[:k], strlist[k+1:]...)
		}
	}
	return strlist
}

//单独值过滤
func removeIds(strlist []string, value string) []string {
	for k, v := range strlist {
		if v == value {
			strlist = append(strlist[:k], strlist[k+1:]...)
		}
	}
	return strlist
}

//列表对列表过滤返回[]string
func removeIdsList(strlist []string, valuelist []string) []string {
	for k, v := range strlist {
		_, found := Find(valuelist, v)
		if found {
			strlist = append(strlist[:k], strlist[k+1:]...)
			return removeIdsList(strlist, valuelist)
		}
	}
	return strlist
}

//列表对列表过滤返回bool
func removeIdsListBool(strlist []string, valuelist []string) bool {
	for _, v := range strlist {
		_, found := Find(valuelist, v)
		if found {
			return true
		}
	}
	return false
}

//获取所有角色列表
func GetRole() ([]*Role, error) {
	var Roles []*Role
	err := db.Order("role_id asc").Find(&Roles).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}

	for _, v := range Roles {
		ids := removeList(strings.Split(v.PsIds, ","))
		for _, vv := range ids {
			intId, _ := strconv.Atoi(vv)
			strLevel := getRootPermission(intId)
			if strLevel == "0" {
				var perms Permission
				err := db.Where("`ps_id` =?", intId).First(&perms).Error
				if err != nil && err != gorm.ErrRecordNotFound {
					fmt.Println("出错")
				}
				var childrenResult PermissionApi
				cRes := childrenResult.getPermissionApi(intId)

				childone := TreeOne{
					PsId:     perms.PsId,
					PsName:   perms.PsName,
					Path:     cRes[0].PsApiPath,
					Children: []*TreeOne{},
				}
				getTreeNodeSelect(intId, ids, &childone)
				v.Children = append(v.Children, &childone)
			}
		}

	}

	return Roles, nil
}

//获取指定id下的角色列表
func GetRoleById(rid int) ([]*Role, error) {
	var Roles []*Role
	err := db.Where("`role_id` =?", rid).First(&Roles).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}

	for _, v := range Roles {
		ids := removeList(strings.Split(v.PsIds, ","))
		for _, vv := range ids {
			intId, _ := strconv.Atoi(vv)
			strLevel := getRootPermission(intId)
			if strLevel == "0" {
				var perms Permission
				err := db.Where("`ps_id` =?", intId).First(&perms).Error
				if err != nil && err != gorm.ErrRecordNotFound {
					fmt.Println("出错")
				}
				var childrenResult PermissionApi
				cRes := childrenResult.getPermissionApi(intId)

				childone := TreeOne{
					PsId:     perms.PsId,
					PsName:   perms.PsName,
					Path:     cRes[0].PsApiPath,
					Children: []*TreeOne{},
				}
				getTreeNodeSelect(intId, ids, &childone)
				v.Children = append(v.Children, &childone)
			}
		}

	}

	return Roles, nil
}

//根据role_id和right_id 删除角色下对应的权限节点id, 返回该角色下新的权限嵌套节点
func DelRoleRightId(roleId int, rightId int) ([]*Role, error) {
	var Roles Role

	if err := db.Select("ps_ids").Where("role_id = ?", roleId).First(&Roles).Error; err != nil {
		fmt.Println(err)
	}
	stringIds := strings.Split(Roles.PsIds, ",")

	ok, err := ExistPermissionByID(rightId)
	if err != nil {
		fmt.Println("发生异常错误")
	}

	if ok {
		//存在下级
		myIds := getAllChildrenIds(rightId, stringIds)
		newStringIds := removeIds(stringIds, strconv.Itoa(rightId))
		updateStringIds := strings.Replace(strings.Trim(fmt.Sprint(removeIdsList(newStringIds, myIds)), "[]"), " ", ",", -1)
		//fmt.Println("整理好准备更新的ids")
		//fmt.Println(updateStringIds)
		//重置IdsList 方便下一次使用
		IdsList = IdsList[:0]
		err := db.Model(&Roles).Where("role_id = ?", roleId).Update("ps_ids", updateStringIds).Error
		if err != nil && err != gorm.ErrRecordNotFound {
			fmt.Println("更新出错")
		}
		stringIds = strings.Split(Roles.PsIds, ",")
		//进入检查阶段
		getFatherId(rightId)
		for _, callV := range CallBackList {
			newV, _ := strconv.Atoi(callV)
			resList := getAllChildrenIds(newV, stringIds)
			if len(resList) == 0 {
				stringIds = removeIds(stringIds, callV)
			}
		}
		CallBackList = CallBackList[:0]
		IdsList = IdsList[:0]
		updateStringIds = strings.Replace(strings.Trim(fmt.Sprint(stringIds), "[]"), " ", ",", -1)
		errs := db.Model(&Roles).Where("role_id = ?", roleId).Update("ps_ids", updateStringIds).Error
		if errs != nil && errs != gorm.ErrRecordNotFound {
			fmt.Println("更新出错")
		}

	} else {
		stringIds = removeIds(stringIds, strconv.Itoa(rightId))
		fmt.Println("打印stringIds原始")
		fmt.Println(stringIds)
		//进入检查阶段
		getFatherId(rightId)
		for _, callV := range CallBackList {
			newV, _ := strconv.Atoi(callV)
			resList := getAllChildrenIds(newV, stringIds)
			if len(resList) == 0 {
				stringIds = removeIds(stringIds, callV)
			}
		}
		CallBackList = CallBackList[:0]
		IdsList = IdsList[:0]

		updateStringIds := strings.Replace(strings.Trim(fmt.Sprint(stringIds), "[]"), " ", ",", -1)
		err := db.Model(&Roles).Where("role_id = ?", roleId).Update("ps_ids", updateStringIds).Error
		if err != nil && err != gorm.ErrRecordNotFound {
			fmt.Println("更新出错")
		}

	}

	//进入返回阶段
	res, err := GetRoleById(roleId)
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	//fmt.Println("res")
	//fmt.Println(res)
	return res, nil
}

// 角色授权更新ids
func UpdateRoles(id int, data interface{}) (*Role, error) {
	var Roles Role
	err := db.Debug().Model(&Roles).Where("role_id = ?", id).Updates(data).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	e := service.Enforcer
	fmt.Println("更新后的角色列表: ", strings.Split(Roles.PsIds, ","))
	PolicyName := getRoleNameString(id)
	//获取该角色下的所有权限策略
	filteredPolicy := e.GetFilteredPolicy(0, PolicyName)
	fmt.Println("filteredPolicy: ", filteredPolicy)
	for _, vcasbin := range filteredPolicy {
		//遍历删除该角色下的所有权限策略
		e.RemovePolicy(vcasbin)
	}
	//删除角色-它会自动删除角色下的用户关系 这个适合真正删除角色的时候用
	//e.DeleteRole(PolicyName)
	for _, v := range strings.Split(Roles.PsIds, ",") {
		IntV, _ := strconv.Atoi(v)
		PolicyList := getPsCAPermission(IntV)
		if len(PolicyList) > 0 {
			//遍历赋予角色新的权限策略
			e.AddPolicy(PolicyName, PolicyList[0], PolicyList[1])
		}
	}
	e.LoadPolicy()

	return &Roles, nil
}
