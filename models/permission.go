package models

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"strconv"
	"strings"
)

var CallBackList []string
var IdsList []string

//权限树的展示结构体
type Tree struct {
	PsId     int         `json:"id"`
	PsName   string      `json:"authName"`
	Path     string      `json:"path"`
	PsPid    interface{} `json:"pid"`
	Children []*Tree     `json:"children,omitempty"`
}

//权限树的展示结构体1
type TreeOne struct {
	PsId     int        `json:"id"`
	PsName   string     `json:"authName"`
	Path     string     `json:"path"`
	Children []*TreeOne `json:"children,omitempty"`
}

//权限数据库映射字段结构体
type Permission struct {
	PsId    int    `gorm:"primary_key" json:"id"`
	PsName  string `json:"authName"`
	PsLevel string `json:"level"`
	PsPid   int    `json:"pid"`
	Path    string `json:"path" gorm:"default:''"`
	PsC     string `json:"-"  gorm:"default:''"`
	PsA     string `json:"-"  gorm:"default:''"`
}

//根据pid返回上级ps_name
func (p *Permission) getPermission(pid int) (ret []*Permission) {
	if err := db.Select("ps_name").Where("ps_id = ?", pid).First(&ret).Error; err != nil {
		fmt.Println(err)
	}
	return
}

//根据id判断是否为根节点
func getRootPermission(id int) string {
	var permissions Permission
	if err := db.Select("ps_level").Where("ps_id = ?", id).First(&permissions).Error; err != nil {
		return ""
	}
	return permissions.PsLevel
}

//根据id返回ps_c,ps_a
func getPsCAPermission(id int) []string {
	var permissions Permission
	var resList []string
	if err := db.Select("ps_c, ps_a").Where("ps_id = ?", id).First(&permissions).Error; err != nil {
		return nil
	}

	if len(permissions.PsC) > 0 {
		resList = append(resList, permissions.PsC)
		resList = append(resList, permissions.PsA)
	}

	return resList
}

//获取所有权限 list
func GetPermissionsList() ([]*Permission, error) {
	var permissions []*Permission

	err := db.Order("ps_id ASC").Find(&permissions).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}

	for _, v := range permissions {
		var childrenResult PermissionApi
		cRes := childrenResult.getPermissionApi(v.PsId)
		v.Path = cRes[0].PsApiPath
	}
	return permissions, nil
}

//获取所有权限Tree
func GetAllPerm() (perms []*Tree) {
	child := Tree{
		PsId:     0,
		PsName:   "",
		Path:     "",
		PsPid:    0,
		Children: []*Tree{},
	}
	err := getTreeNode(0, &child)
	if err != nil {
		return
	}
	perms = append(perms, &child)
	return perms[0].Children
}

// 判断是否存在下级
func ExistPermissionByID(id int) (bool, error) {
	var permission Permission
	err := db.Select("ps_id").Where("ps_pid = ?", id).First(&permission).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}
	if permission.PsId > 0 {
		return true, nil
	}
	return false, nil
}

// 返回节点名称
func GetPermissionApiPathByID(id int) string {
	var permissionApi PermissionApi
	err := db.Select("ps_api_path").Where("ps_id = ?", id).First(&permissionApi).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return "错误"
	}
	return permissionApi.PsApiPath
}

//递归获取子节点
func getTreeNode(pId int, treeNode *Tree) error {
	var perms []Permission
	err := db.Where("`ps_pid` =?", pId).Find(&perms).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return err
	}

	for i := 0; i < len(perms); i++ {
		perms[i].Path = GetPermissionApiPathByID(perms[i].PsId)
		child := Tree{
			perms[i].PsId,
			perms[i].PsName,
			perms[i].Path,
			perms[i].PsPid,
			[]*Tree{},
		}

		ok, _ := ExistPermissionByID(perms[i].PsId)
		if !ok {
			//调用方法将改psid所有上级id添加到切片
			getFatherId(child.PsId)
			//将切片转换成xx,xx字符串
			child.PsPid = strings.Replace(strings.Trim(fmt.Sprint(CallBackList), "[]"), " ", ",", -1)
			//清空CallBackList 目的为了下次循环的节点使用
			CallBackList = CallBackList[:0]
		}

		treeNode.Children = append(treeNode.Children, &child)
		getTreeNode(perms[i].PsId, &child)
	}
	return err
}

//将某个节点所有上级id添加到CallBackList切片
func getFatherId(id int) string {
	var perms Permission
	err := db.Select("ps_pid").Where("`ps_id` =?", id).Find(&perms).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return "错误"
	}
	if perms.PsPid != 0 {
		CallBackList = append(CallBackList, strconv.Itoa(perms.PsPid))
	} else {
		return ""
	}
	return getFatherId(perms.PsPid)
}

//获取所有下级节点id
func getAllChildrenIds(pId int, idList []string) []string {
	var perms []*Permission
	err := db.Where("`ps_pid` =?", pId).Find(&perms).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		fmt.Println("查询错误")
	}

	for i := 0; i < len(perms); i++ {
		_, found := Find(idList, strconv.Itoa(perms[i].PsId))
		if found {
			IdsList = append(IdsList, strconv.Itoa(perms[i].PsId))
			getAllChildrenIds(perms[i].PsId, idList)
		}
	}
	return IdsList
}

//递归获取子节点摘选
func getTreeNodeSelect(pId int, idList []string, treeNode *TreeOne) error {
	var perms []Permission
	err := db.Where("`ps_pid` =?", pId).Find(&perms).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return err
	}

	for i := 0; i < len(perms); i++ {
		_, found := Find(idList, strconv.Itoa(perms[i].PsId))
		if found {
			perms[i].Path = GetPermissionApiPathByID(perms[i].PsId)
			childone := TreeOne{
				perms[i].PsId,
				perms[i].PsName,
				perms[i].Path,
				[]*TreeOne{},
			}
			treeNode.Children = append(treeNode.Children, &childone)
			getTreeNodeSelect(perms[i].PsId, idList, &childone)
		}

	}
	return err
}

func Find(slice []string, val string) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}
