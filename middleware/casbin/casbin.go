package casbin

import (
	"fmt"
	"github.com/gin-gonic/gin"
	pkge "go-gin-example/pkg/e"
	service "go-gin-example/service/casbin_service"
	"net/http"
)

//拦截器
func Authorize() gin.HandlerFunc {

	return func(c *gin.Context) {
		var code int
		var data interface{}
		// var e *casbin.Enforcer
		e := service.Enforcer
		//从DB加载策略
		e.LoadPolicy()

		//获取请求的URI
		obj := c.Request.URL.RequestURI()
		fmt.Println("obj:", obj)
		//获取请求方法
		act := c.Request.Method
		fmt.Println("act:", act)
		//从中间件中取出用户名
		sub, _ := c.Get("username")
		fmt.Println("sub:", sub)

		code = pkge.SUCCESS

		//判断策略中是否存在
		if ok, _ := e.Enforce(sub, obj, act); ok {
			fmt.Println("恭喜您,权限验证通过")
		} else {
			code = pkge.INVALPERMISSION
			fmt.Println("很遗憾,权限验证没有通过")
		}

		if code != pkge.SUCCESS {
			c.JSON(http.StatusUnauthorized, gin.H{
				"code": code,
				"msg":  pkge.GetMsg(code),
				"data": data,
			})

			c.Abort()
			return
		}

		c.Next()
	}
}
