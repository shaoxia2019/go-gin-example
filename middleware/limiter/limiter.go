package limiter

import (
	"github.com/gin-gonic/gin"
	libredis "github.com/go-redis/redis/v7"
	limiter "github.com/ulule/limiter/v3"
	mgin "github.com/ulule/limiter/v3/drivers/middleware/gin"
	sredis "github.com/ulule/limiter/v3/drivers/store/redis"
	"go-gin-example/pkg/setting"
	"log"
)

func Limiter() gin.HandlerFunc {
	rate, err := limiter.NewRateFromFormatted(setting.RedisSetting.LimiterPolicy)
	if err != nil {
		log.Fatal(err)
	}

	// Create a redis client.
	option, err := libredis.ParseURL("redis://" + setting.RedisSetting.Host + "/0")
	option.Password = setting.RedisSetting.Password

	if err != nil {
		log.Fatal(err)
	}

	client := libredis.NewClient(option)

	// Create a store with the redis client.
	store, err := sredis.NewStoreWithOptions(client, limiter.StoreOptions{
		Prefix:   "limiter_gin_example",
		MaxRetry: 3,
	})
	if err != nil {
		log.Fatal(err)
	}

	// Create a new middleware with the limiter instance.
	middleware := mgin.NewMiddleware(limiter.New(store, rate))
	return middleware
}
