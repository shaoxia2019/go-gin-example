package user_service

import (
	"fmt"
	"go-gin-example/models"
	"go-gin-example/pkg/util"
	"strconv"
	"strings"
	"time"
)

type NullType byte

const (
	_ NullType = iota
	// IsNull the same as `is null`
	IsNull
	// IsNotNull the same as `is not null`
	IsNotNull
)

type User struct {
	ID       int
	State    int
	MgName   string
	RoleId   string
	MgPwd    string
	MgEmail  string
	MgMobile string
	PageNum  int
	PageSize int
	Query    string
	Order    string
}

type UserState struct {
	ID       int    `json:"id"`
	Rid      string `json:"rid"`
	UserName string `json:"username"`
	Mobile   string `json:"mobile"`
	Email    string `json:"email"`
	MgState  int    `json:"mg_state"`
}

type BackUserEdit struct {
	ID       int    `json:"id"`
	UserName string `json:"username"`
	RoleId   string `json:"role_id"`
	Mobile   string `json:"mobile"`
	Email    string `json:"email"`
}

type BackUserAdd struct {
	ID         int       `json:"id"`
	Username   string    `json:"username"`
	Mobile     string    `json:"mobile"`
	Email      string    `json:"email"`
	RoleId     string    `json:"role_id"`
	CreateTime time.Time `json:"create_time"`
}

type BackUserByID struct {
	ID       int    `json:"id"`
	RId      string `json:"rid"`
	Username string `json:"username"`
	Mobile   string `json:"mobile"`
	Email    string `json:"email"`
}

func (u *User) ExistByName() (bool, error) {
	return models.ExistManagerByName(u.MgName)
}

//添加用户
func (u *User) Add() (interface{}, error) {
	user := map[string]interface{}{
		"mg_name":   u.MgName,
		"mg_pwd":    util.EncodeMD5(u.MgPwd),
		"mg_email":  u.MgEmail,
		"mg_mobile": u.MgMobile,
	}
	users, err := models.AddManager(user)
	if err != nil {
		return nil, err
	}
	usersResults := BackUserAdd{
		ID:         users.MgId,
		Username:   users.MgName,
		Mobile:     users.MgMobile,
		Email:      users.MgEmail,
		RoleId:     users.RoleId,
		CreateTime: users.CreatedAt,
	}
	return usersResults, nil
}

// sql build where
func whereBuild(where map[string]interface{}) (whereSQL string, vals []interface{}, err error) {
	for k, v := range where {
		ks := strings.Split(k, " ")
		if len(ks) > 2 {
			return "", nil, fmt.Errorf("Error in query condition: %s. ", k)
		}

		if k != "order" {
			if whereSQL != "" {
				whereSQL += " AND "
			}
		}
		strings.Join(ks, ",")
		switch len(ks) {
		case 1:
			//fmt.Println(reflect.TypeOf(v))
			switch v := v.(type) {
			case NullType:
				if v == IsNotNull {
					whereSQL += fmt.Sprint(k, " IS NOT NULL")
				} else {
					whereSQL += fmt.Sprint(k, " IS NULL")
				}
			default:
				if k != "order" {
					whereSQL += fmt.Sprint(k, "=?")
				}
				vals = append(vals, v)
			}
			break
		case 2:
			k = ks[0]
			switch ks[1] {
			case "=":
				if k != "order" {
					whereSQL += fmt.Sprint(k, "=?")
				}
				vals = append(vals, v)
				break
			case ">":
				whereSQL += fmt.Sprint(k, ">?")
				vals = append(vals, v)
				break
			case ">=":
				whereSQL += fmt.Sprint(k, ">=?")
				vals = append(vals, v)
				break
			case "<":
				whereSQL += fmt.Sprint(k, "<?")
				vals = append(vals, v)
				break
			case "<=":
				whereSQL += fmt.Sprint(k, "<=?")
				vals = append(vals, v)
				break
			case "!=":
				whereSQL += fmt.Sprint(k, "!=?")
				vals = append(vals, v)
				break
			case "<>":
				whereSQL += fmt.Sprint(k, "!=?")
				vals = append(vals, v)
				break
			case "in":
				whereSQL += fmt.Sprint(k, " in (?)")
				vals = append(vals, v)
				break
			case "like":
				whereSQL += fmt.Sprint(k, " like ?")
				vals = append(vals, v)
			}
			break
		}
	}
	return
}

func (u *User) ExistByID() (bool, error) {
	return models.ExistManagerByID(u.ID)
}

//验证role是否存在
func (u *User) CheckRoleByID() (bool, error) {
	RoleIdList := strings.Split(u.RoleId, ",")
	fmt.Println("分配角色列表")
	fmt.Println(RoleIdList)
	for _, v := range RoleIdList {
		IntV, _ := strconv.Atoi(v)
		res, _ := models.ExistRoleNewByID(IntV)
		if !res {
			return false, nil
		}
	}
	return true, nil
}

//修改用户
func (u *User) EditAll() (interface{}, error) {
	data := make(map[string]interface{})
	data["mg_email"] = u.MgEmail
	data["mg_mobile"] = u.MgMobile
	users, err := models.EditManager(u.ID, data)
	if err != nil {
		return nil, err
	}
	usersResults := BackUserEdit{
		ID:       users.MgId,
		UserName: users.MgName,
		RoleId:   users.RoleId,
		Mobile:   users.MgMobile,
		Email:    users.MgEmail,
	}

	return usersResults, nil
}

//分配用户角色
func (u *User) SetUserRole() (interface{}, error) {
	data := make(map[string]interface{})
	data["role_id"] = u.RoleId
	users, err := models.SetRoleManager(u.ID, data)
	if err != nil {
		return nil, err
	}
	usersResults := BackUserEdit{
		ID:       users.MgId,
		UserName: users.MgName,
		RoleId:   users.RoleId,
		Mobile:   users.MgMobile,
		Email:    users.MgEmail,
	}

	return usersResults, nil
}

//修改用户状态
func (u *User) Edit() (interface{}, error) {
	data := make(map[string]interface{})
	data["mg_state"] = u.State
	users, err := models.EditManager(u.ID, data)
	if err != nil {
		return nil, err
	}
	usersResults := UserState{
		ID:       users.MgId,
		Rid:      users.RoleId,
		UserName: users.MgName,
		Mobile:   users.MgMobile,
		Email:    users.MgEmail,
		MgState:  users.MgState,
	}

	return usersResults, nil
}

func (u *User) Get() (interface{}, error) {
	users, err := models.GetManagerSingle(u.ID)
	if err != nil {
		return nil, err
	}
	usersResults := BackUserByID{
		ID:       users.MgId,
		RId:      users.RoleId,
		Username: users.MgName,
		Mobile:   users.MgMobile,
		Email:    users.MgEmail,
	}

	return usersResults, nil
}

func (u *User) GetAll() ([]*models.Manager, error) {
	users, err := models.GetManager(u.PageNum, u.PageSize, u.getMaps())
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (u *User) Count() (int, error) {
	return models.GetManagerTotal(u.getMaps())
}

func (u *User) getMaps() map[string]interface{} {
	likeStr := "%" + u.Query + "%"

	comMaps := make(map[string]interface{})
	if u.Query != "" {
		comMaps["mg_name like"] = likeStr
	}
	if u.Order != "" {
		comMaps["order"] = u.Order
	} else {
		comMaps["order"] = "asc"
	}
	//comMaps["mg_id in"] = []int{500, 502, 503}

	cond, vals, err := whereBuild(comMaps)
	if err != nil {
		return nil
	}

	outMaps := make(map[string]interface{})
	outMaps["whereSQL"] = cond
	outMaps["vals"] = vals
	fmt.Println("打印出参查询条件")
	fmt.Println(outMaps)

	return outMaps
}

func (u *User) Delete() error {
	return models.DeleteManger(u.ID)
}
