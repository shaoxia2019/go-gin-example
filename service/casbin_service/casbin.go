package casbin_service

import (
	"fmt"
	"github.com/casbin/casbin/v2"
	gormadapter "github.com/casbin/gorm-adapter/v2"
	"go-gin-example/pkg/setting"
)

var Enforcer *casbin.Enforcer

// 初始化casbin
func CasbinSetup() {
	a, err := gormadapter.NewAdapter(setting.DatabaseSetting.Type, fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		setting.DatabaseSetting.User,
		setting.DatabaseSetting.Password,
		setting.DatabaseSetting.Host,
		setting.DatabaseSetting.Name), true)
	if err != nil {
		fmt.Printf("连接数据库错误: %v", err)
		return
	}
	e, err := casbin.NewEnforcer("conf/rbac_models.conf", a)
	if err != nil {
		fmt.Printf("初始化casbin错误: %v", err)
		return
	}

	Enforcer = e
}
