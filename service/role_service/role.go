package role_service

import (
	"go-gin-example/models"
)

type Role struct {
	ID       int
	RoleName string
	RoleDesc string
	RoleId   int
	RightId  int
	RoleIds  string
}

func (r *Role) Get() (interface{}, error) {
	roles, err := models.GetRole()
	if err != nil {
		return nil, err
	}
	return roles, nil
}

//删除角色的权限
func (r *Role) DelRRId() interface{} {
	roles, _ := models.DelRoleRightId(r.RoleId, r.RightId)
	return roles[0].Children
}

//是否存在该角色以及角色下的权限
func (r *Role) ExistByID() (bool, error) {
	return models.ExistRoleByID(r.RoleId, r.RightId)
}

//是否存在该角色
func (r *Role) ExistNewByID() (bool, error) {
	return models.ExistRoleNewByID(r.RoleId)
}

//角色授权(给角色设置权限)
func (r *Role) SetRole() (interface{}, error) {
	data := make(map[string]interface{})
	data["ps_ids"] = r.RoleIds
	//fmt.Println("打印更新数据")
	//fmt.Println(data)
	models.UpdateRoles(r.RoleId, data)
	return "", nil
}
