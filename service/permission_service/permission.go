package permission_service

import (
	"go-gin-example/models"
)

type PermissionApi struct {
	PsId       int
	Name       string
	PsApiPath  string
	PsApiOrder int
	PageNum    int
	PageSize   int
}

type Permission struct {
	PsId    int
	PsName  string
	PsLevel string
	PsPid   int
	Path    string
	Type    string
}

//获取所有权限数据 列表
func (p *Permission) GetPermissionsList() ([]*models.Permission, error) {
	var (
		perMissions []*models.Permission
	)
	perMissions, err := models.GetPermissionsList()
	if err != nil {
		return nil, err
	}
	return perMissions, nil
}

//获取所有权限数据 tree
func (p *Permission) GetPermissionsTree() (perms []*models.Tree) {
	return models.GetAllPerm()
}

func (t *PermissionApi) GetAll() ([]*models.PermissionApi, error) {
	var (
		perMissionApis []*models.PermissionApi
	)
	perMissionApis, err := models.GetPermissions(t.PageNum, t.PageSize, t.getMaps())
	if err != nil {
		return nil, err
	}
	return perMissionApis, nil
}

func (t *PermissionApi) getMaps() map[string]interface{} {
	maps := make(map[string]interface{})
	return maps
}
