package routers

import (
	"github.com/gin-gonic/gin"
	"go-gin-example/middleware/casbin"
	"go-gin-example/middleware/cors"
	"go-gin-example/middleware/jwt"
	"go-gin-example/middleware/limiter"
	"go-gin-example/pkg/export"
	"go-gin-example/pkg/qrcode"
	"go-gin-example/pkg/upload"
	"go-gin-example/routers/api"
	"go-gin-example/routers/api/v1"
	"net/http"
)

// InitRouter initialize routing information
func InitRouter() *gin.Engine {

	r := gin.New()
	r.ForwardedByClientIP = true
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(cors.Cors())

	r.StaticFS("/export", http.Dir(export.GetExcelFullPath()))
	r.StaticFS("/upload/images", http.Dir(upload.GetImageFullPath()))
	r.StaticFS("/qrcode", http.Dir(qrcode.GetQrCodeFullPath()))

	r.POST("/auth", api.GetAuth)
	r.POST("/upload", api.UploadImage)

	apiv1 := r.Group("/api/v1")
	apiv1.Use(limiter.Limiter())
	apiv1.Use(jwt.JWT())

	apiv1.Use(casbin.Authorize())
	{
		//获取标签列表
		apiv1.GET("/tags", v1.GetTags)
		//新建标签
		apiv1.POST("/tags", v1.AddTag)
		//更新指定标签
		apiv1.PUT("/tags/:id", v1.EditTag)
		//删除指定标签
		apiv1.DELETE("/tags/:id", v1.DeleteTag)
		//获取文章列表
		apiv1.GET("/articles", v1.GetArticles)
		//获取指定文章
		apiv1.GET("/articles/:id", v1.GetArticle)
		//新建文章
		apiv1.POST("/articles", v1.AddArticle)
		//更新指定文章
		apiv1.PUT("/articles/:id", v1.EditArticle)
		//删除指定文章
		apiv1.DELETE("/articles/:id", v1.DeleteArticle)
		//生成文章海报
		apiv1.POST("/articles/poster/generate", v1.GenerateArticlePoster)
		//获取左侧菜单
		apiv1.GET("/menus", v1.GetPermissions)
		//添加用户
		apiv1.POST("/users", v1.AddUser)
		//删除指定标签
		apiv1.DELETE("/users/:id", v1.DeleteManager)
		//修改用户
		apiv1.PUT("/users/:id", v1.EditManager)
		//更新用户状态
		apiv1.PUT("/users/:id/state/:state", v1.EditManagerState)
		//分配用户角色
		apiv1.PUT("/users/:id/role", v1.SetUserRole)
		//获取用户列表
		apiv1.GET("/users", v1.GetUsers)
		//获取指定用户
		apiv1.GET("/users/:id", v1.GetUser)
		//获取所有权限
		apiv1.GET("/rights/:type", v1.GetPermissionList)
		//删除角色权限
		apiv1.DELETE("/roles/:roleId/rights/:rightId", v1.DelRoleRight)
		//角色授权
		apiv1.POST("/roles/:roleId/rights", v1.SetRoles)
		//获取角色列表
		apiv1.GET("/roles", v1.GetRoles)
	}

	r.Use(jwt.JWT())
	{
		//导出标签
		r.POST("/tags/export", v1.ExportTag)
		//导入标签
		r.POST("/tags/import", v1.ImportTag)
	}

	return r
}
