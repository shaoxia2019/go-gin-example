package v1

import (
	"github.com/unknwon/com"
	"net/http"

	"github.com/gin-gonic/gin"
	"go-gin-example/pkg/app"
	"go-gin-example/pkg/e"
	"go-gin-example/service/role_service"
)

//删除角色权限结构体
type DelRRForm struct {
	RoleId  int `json:"roleId" valid:"Required;Min(1)"`
	RightId int `json:"rightId" valid:"Required;Min(1)"`
}

//删除角色权限
func DelRoleRight(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = DelRRForm{
			RoleId:  com.StrTo(c.Param("roleId")).MustInt(),
			RightId: com.StrTo(c.Param("rightId")).MustInt(),
		}
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != 80088 {
		if errCode != e.SUCCESS {
			appG.Response(httpCode, errCode, nil)
			return
		}
	} else {
		return
	}

	roleService := role_service.Role{
		RoleId:  form.RoleId,
		RightId: form.RightId,
	}

	exists, err := roleService.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EXIST_USER_FAIL, nil)
		return
	}
	if !exists {
		appG.Response(http.StatusOK, e.ERROR_NOT_EXIST_USER, nil)
		return
	}

	roles := roleService.DelRRId()
	appG.Response(http.StatusOK, e.SUCCESS, roles)
}

//角色授权结构体
type SetRoleForm struct {
	RoleId  int    `json:"roleId" valid:"Required;Min(1)"`
	RoleIds string `json:"rids" valid:"Required;MinSize(1)"`
}

func SetRoles(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = SetRoleForm{
			RoleId:  com.StrTo(c.Param("roleId")).MustInt(),
		}
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != 80088 {
		if errCode != e.SUCCESS {
			appG.Response(httpCode, errCode, nil)
			return
		}
	} else {
		return
	}

	roleService := role_service.Role{
		RoleId:  form.RoleId,
		RoleIds: form.RoleIds,
	}

	exists, err := roleService.ExistNewByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EXIST_USER_FAIL, nil)
		return
	}
	if !exists {
		appG.Response(http.StatusOK, e.ERROR_NOT_EXIST_USER, nil)
		return
	}

	roles, _ := roleService.SetRole()
	appG.Response(http.StatusOK, e.SUCCESS, roles)

}

//获取角色列表
func GetRoles(c *gin.Context) {
	appG := app.Gin{C: c}
	roleService := role_service.Role{
		ID:       0,
		RoleName: "",
		RoleDesc: "",
	}
	roles, err := roleService.Get()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_ARTICLES_FAIL, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, roles)
}
