package v1

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"go-gin-example/models"
	"go-gin-example/pkg/app"
	"go-gin-example/pkg/e"
	"go-gin-example/pkg/setting"
	"go-gin-example/pkg/util"
	"go-gin-example/service/permission_service"
	"net/http"
)

func GetPermissions(c *gin.Context) {
	appG := app.Gin{C: c}
	permissionService := permission_service.PermissionApi{
		PageNum:  util.GetPage(c),
		PageSize: setting.AppSetting.PageSize,
	}
	var (
		permissions []*models.PermissionApi
		err         error
	)

	permissions, err = permissionService.GetAll()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_TAGS_FAIL, nil)
		return
	}

	//appG.Response(http.StatusOK, e.SUCCESS, map[string]interface{}{
	//	"data": permissions,
	//})

	appG.Response(http.StatusOK, e.SUCCESS, permissions)

}

//所有权限结构体
type GetPermissionForm struct {
	Type string `json:"type" valid:"Required;Alpha"`
}

func GetPermissionList(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = GetPermissionForm{
			Type: c.Param("type"),
		}
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != 80088 {
		if errCode != e.SUCCESS {
			appG.Response(httpCode, errCode, nil)
			return
		}
	} else {
		return
	}

	permissionService := permission_service.Permission{
		Type: form.Type,
	}
	var (
		permissions []*models.Permission
		err         error
	)

	switch permissionService.Type {
	case "list":
		permissions, err = permissionService.GetPermissionsList()
		if err != nil {
			appG.Response(http.StatusInternalServerError, e.ERROR_GET_TAGS_FAIL, nil)
			return
		}
		appG.Response(http.StatusOK, e.SUCCESS, permissions)
	case "tree":
		fmt.Println("是tree")
		permissions := permissionService.GetPermissionsTree()
		appG.Response(http.StatusOK, e.SUCCESS, permissions)


	default:
		fmt.Println("无效的输入")
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_TAGS_FAIL, nil)
		return
	}

}
