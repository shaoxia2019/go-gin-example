package v1

import (
	"go-gin-example/pkg/setting"
	"net/http"

	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"

	"go-gin-example/pkg/app"
	"go-gin-example/pkg/e"
	"go-gin-example/pkg/util"
	"go-gin-example/service/user_service"
)

//获取指定用户结构体
type GetUserByIdForm struct {
	ID int `json:"id" alias:"用户" valid:"Required;Min(1);"`
}

//获取指定用户
func GetUser(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = GetUserByIdForm{
			ID: com.StrTo(c.Param("id")).MustInt(),
		}
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != 80088 {
		if errCode != e.SUCCESS {
			appG.Response(httpCode, errCode, nil)
			return
		}
	} else {
		return
	}

	userService := user_service.User{
		ID: form.ID,
	}
	exists, err := userService.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_CHECK_EXIST_ARTICLE_FAIL, nil)
		return
	}
	if !exists {
		appG.Response(http.StatusOK, e.ERROR_NOT_EXIST_USER, nil)
		return
	}

	user, err := userService.Get()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_ARTICLE_FAIL, nil)
		return
	}
	appG.Response(http.StatusOK, e.SUCCESS, user)
}

//获取用户列表
func GetUsers(c *gin.Context) {
	appG := app.Gin{C: c}
	valid := validation.Validation{}
	pagenum := com.StrTo(c.Query("pagenum")).MustInt()
	pagesize := com.StrTo(c.Query("pagesize")).MustInt()
	query := c.Query("query")
	order := c.Query("order")
	if order != "" {
		if order != "asc" {
			if order != "desc" {
				appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
				return
			}
		}
	}

	valid.Min(pagenum, 1, "pagenum")
	valid.Min(pagesize, 1, "pagesize")
	if pagesize > setting.AppSetting.PageSize {
		appG.Response(http.StatusBadRequest, e.OVER_PAGESIZE, nil)
		return
	}
	if valid.HasErrors() {
		app.MarkErrors(valid.Errors)
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
		return
	}

	userService := user_service.User{
		PageNum:  util.GetPageNum(c),
		PageSize: util.GetPageSize(c),
		Query:    query,
		Order:    order,
	}

	total, err := userService.Count()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_COUNT_ARTICLE_FAIL, nil)
		return
	}

	users, err := userService.GetAll()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_GET_ARTICLES_FAIL, nil)
		return
	}

	data := make(map[string]interface{})
	data["users"] = users
	data["pagenum"] = pagenum
	data["total"] = total

	appG.Response(http.StatusOK, e.SUCCESS, data)
}

//添加用户结构体
type AddUserForm struct {
	Username string `json:"username" alias:"用户名" valid:"Required;MinSize(3);MaxSize(10)"`
	Password string `json:"password" alias:"用户密码" valid:"Required;MinSize(6);MaxSize(15)"`
	Email    string `json:"email" alias:"用户email"`
	Mobile   string `json:"mobile" alias:"用户mobile"`
}

//添加用户
func AddUser(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form AddUserForm
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != 80088 {
		if errCode != e.SUCCESS {
			appG.Response(httpCode, errCode, nil)
			return
		}
	} else {
		return
	}

	userService := user_service.User{
		MgName:   form.Username,
		MgPwd:    form.Password,
		MgEmail:  form.Email,
		MgMobile: form.Mobile,
	}

	exists, err := userService.ExistByName()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EXIST_USER_FAIL, nil)
		return
	}
	if exists {
		appG.Response(http.StatusOK, e.ERROR_EXIST_USER, nil)
		return
	}

	users, err := userService.Add()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EDIT_USERSTATE_FAIL, nil)
		return
	}
	appG.Response(http.StatusCreated, e.CREATED, users)
}

//修改用户结构体
type EditUserForm struct {
	ID     int    `json:"id" valid:"Required;Min(1)"`
	Email  string `json:"email" valid:"Required;Email"`
	Mobile string `json:"mobile" valid:"Required;Mobile"`
}

//修改用户
func EditManager(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = EditUserForm{
			ID: com.StrTo(c.Param("id")).MustInt(),
		}
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != 80088 {
		if errCode != e.SUCCESS {
			appG.Response(httpCode, errCode, nil)
			return
		}
	} else {
		return
	}

	userService := user_service.User{
		ID:       form.ID,
		MgEmail:  form.Email,
		MgMobile: form.Mobile,
	}

	exists, err := userService.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EXIST_USER_FAIL, nil)
		return
	}

	if !exists {
		appG.Response(http.StatusOK, e.ERROR_NOT_EXIST_USER, nil)
		return
	}

	users, err := userService.EditAll()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EDIT_USERSTATE_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, users)
}

//分配用户角色结构体
type SetUserRoleForm struct {
	ID  int    `json:"id" valid:"Required;Min(1)"`
	Rid string `json:"rid" valid:"Required;MinSize(1)"`
}

//分配用户角色
func SetUserRole(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = SetUserRoleForm{
			ID: com.StrTo(c.Param("id")).MustInt(),
		}
	)


	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != 80088 {
		if errCode != e.SUCCESS {
			appG.Response(httpCode, errCode, nil)
			return
		}
	} else {
		return
	}

	userService := user_service.User{
		ID:     form.ID,
		RoleId: form.Rid,
	}


	exists, err := userService.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EXIST_USER_FAIL, nil)
		return
	}

	if !exists {
		appG.Response(http.StatusOK, e.ERROR_NOT_EXIST_USER, nil)
		return
	}

	existsRole, _ := userService.CheckRoleByID()
	if !existsRole {
		appG.Response(http.StatusOK, e.ERROR_NOT_EXIST_SETROLE, nil)
		return
	}


	users, err := userService.SetUserRole()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EDIT_USERSTATE_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, users)
}

//更新用户状态结构体
type EditManagerStateForm struct {
	ID    int `json:"id" valid:"Required;Min(1)"`
	State int `json:"state" valid:"Range(0,1)"`
}

//更新用户状态
func EditManagerState(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = EditManagerStateForm{
			ID:    com.StrTo(c.Param("id")).MustInt(),
			State: com.StrTo(c.Param("state")).MustInt(),
		}
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != 80088 {
		if errCode != e.SUCCESS {
			appG.Response(httpCode, errCode, nil)
			return
		}
	} else {
		return
	}

	userService := user_service.User{
		ID:    form.ID,
		State: form.State,
	}

	exists, err := userService.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EXIST_USER_FAIL, nil)
		return
	}

	if !exists {
		appG.Response(http.StatusOK, e.ERROR_NOT_EXIST_USER, nil)
		return
	}

	users, err := userService.Edit()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EDIT_USERSTATE_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, users)
}

//删除用户结构体
type DeleteManagerForm struct {
	ID int `json:"id" valid:"Required;Min(1)"`
}

//删除用户
func DeleteManager(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
		form = DeleteManagerForm{
			ID: com.StrTo(c.Param("id")).MustInt(),
		}
	)

	httpCode, errCode := app.BindAndValid(c, &form)
	if errCode != 80088 {
		if errCode != e.SUCCESS {
			appG.Response(httpCode, errCode, nil)
			return
		}
	} else {
		return
	}

	userService := user_service.User{
		ID: form.ID,
	}
	exists, err := userService.ExistByID()
	if err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_EXIST_USER_FAIL, nil)
		return
	}

	if !exists {
		appG.Response(http.StatusOK, e.ERROR_NOT_EXIST_USER, nil)
		return
	}

	if err := userService.Delete(); err != nil {
		appG.Response(http.StatusInternalServerError, e.ERROR_DELETE_TAG_FAIL, nil)
		return
	}

	appG.Response(http.StatusOK, e.SUCCESS, nil)
}
