package api

import (
	"github.com/astaxie/beego/validation"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"go-gin-example/models"
	"go-gin-example/pkg/e"
	"go-gin-example/pkg/logging"
	"go-gin-example/pkg/util"
	"net/http"
)

type Claims struct {
	Username string `json:"username"`
	Encrystr string `json:"encrystr"`
	jwt.StandardClaims
}

//验证用户结构体
type CheckUserForm struct {
	Username string `json:"username" alias:"用户名" valid:"Required;MinSize(3);MaxSize(10)"`
	Password string `json:"password" alias:"用户密码" valid:"Required;MinSize(6);MaxSize(15)"`
}

func GetAuth(c *gin.Context) {
	var udata CheckUserForm
	code := e.INVALID_PARAMS
	//校验json包体格式
	if err := c.ShouldBindJSON(&udata); err != nil {
		c.JSON(code, gin.H{
			"code": code,
			"msg":  e.GetMsg(code),
			"data": make(map[string]string),
		})
		return
	}

	//验证器验证
	username := udata.Username
	password := util.EncodeMD5(string(udata.Password))
	data := make(map[string]interface{})

	valid := validation.Validation{}
	valid.Required(username, "username").Message("名称不能为空")
	valid.Required(password, "password").Message("密码不能为空")

	if !valid.HasErrors() {
		isExist := models.CheckAuth(username, password)
		if isExist != "no" {
			token, err := util.GenerateToken(username, isExist)
			if err != nil {
				code = e.ERROR_AUTH_TOKEN
			} else {
				data["token"] = token
				code = e.SUCCESS
			}
		} else {
			code = e.ERROR_AUTH
		}
	} else {
		for _, err := range valid.Errors {
			logging.Info(err.Key, err.Message)
			c.JSON(http.StatusOK, gin.H{
				"code": code,
				"msg":  err.Key + " " + err.Message,
				"data": make(map[string]interface{}),
			})
			return
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  e.GetMsg(code),
		"data": data,
	})
}
