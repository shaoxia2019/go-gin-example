package util

import (
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
	"go-gin-example/pkg/setting"
)

//GetPage get page parameters
func GetPage(c *gin.Context) int {
	result := 0
	page := com.StrTo(c.Query("page")).MustInt()
	if page > 0 {
		result = (page - 1) * setting.AppSetting.PageSize
	}
	return result
}

func GetPageNum(c *gin.Context) int {
	PageNum := com.StrTo(c.Query("pagenum")).MustInt()
	return PageNum
}

func GetPageSize(c *gin.Context) int {
	PageSize := com.StrTo(c.Query("pagesize")).MustInt()
	//
	//if PageSize > setting.AppSetting.PageSize {
	//	appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
	//}
	return PageSize
}
