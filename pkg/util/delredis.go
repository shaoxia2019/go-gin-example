package util

import (
	"go-gin-example/pkg/gredis"
	"go-gin-example/pkg/logging"
)

func Delr(ttype string) {
	key := ttype + "_LIST"
	err := gredis.LikeDeletes(key)
	if err != nil {
		logging.Info(err)
	}
}
