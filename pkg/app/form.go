package app

import (
	"fmt"
	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"go-gin-example/pkg/e"
	"go-gin-example/pkg/logging"
	"net/http"
	"reflect"
)

// BindAndValid binds and validates data
func BindAndValid(c *gin.Context, form interface{}) (int, int) {
	setVerifyMessage()

	err := c.ShouldBind(form)
	if err != nil {
		return http.StatusBadRequest, e.INVALID_PARAMS
	}

	valid := validation.Validation{}
	check, err := valid.Valid(form)
	if err != nil {
		return http.StatusInternalServerError, e.ERROR
	}
	if !check {
		st := reflect.TypeOf(form).Elem()
		for _, err := range valid.Errors {
			//获取验证的字段名和提示信息的别名
			filed, _ := st.FieldByName(err.Field)
			var title = filed.Tag.Get("json")
			var alias = filed.Tag.Get("alias")
			fmt.Println(alias + " " + title + " " + err.Message)
			c.JSON(http.StatusBadRequest, gin.H{
				"code": http.StatusBadRequest,
				"msg":  alias + " " + title + " " + err.Message,
			})
			logging.Info(alias + " " + title + " " + err.Message)
			fmt.Println("啥错误")
			fmt.Println(alias + " " + title + " " + err.Message)
			return http.StatusBadRequest, 80088
		}
	}

	return http.StatusOK, e.SUCCESS
}

// 设置表单验证messages
func setVerifyMessage() {
	var MessageTmpls = map[string]string{
		"Required":     "必填项",
		"Min":          "最小值 为 %d",
		"Max":          "最大值 为 %d",
		"Range":        "范围 为 %d 到 %d",
		"MinSize":      "最短长度 为 %d",
		"MaxSize":      "最大长度 为 %d",
		"Length":       "长度必须 为 %d",
		"Alpha":        "必须是有效的字母",
		"Numeric":      "必须是有效的数字",
		"AlphaNumeric": "必须是有效的字母或数字",
		"Match":        "必须匹配 %s",
		"NoMatch":      "必须不匹配 %s",
		"AlphaDash":    "必须是有效的字母、数字或连接符号(-_)",
		"Email":        "必须是有效的电子邮件地址",
		"IP":           "必须是有效的IP地址",
		"Base64":       "必须是有效的base64字符",
		"Mobile":       "必须是有效的手机号码",
		"Tel":          "必须是有效的电话号码",
		"Phone":        "必须是有效的电话或移动电话号码",
		"ZipCode":      "必须是有效的邮政编码",
	}
	validation.SetDefaultMessage(MessageTmpls)
}

//StructTag 可用的验证函数：
//
//Required 不为空，即各个类型要求不为其零值
//Min(min int) 最小值，有效类型：int，其他类型都将不能通过验证
//Max(max int) 最大值，有效类型：int，其他类型都将不能通过验证
//Range(min, max int) 数值的范围，有效类型：int，他类型都将不能通过验证
//MinSize(min int) 最小长度，有效类型：string slice，其他类型都将不能通过验证
//MaxSize(max int) 最大长度，有效类型：string slice，其他类型都将不能通过验证
//Length(length int) 指定长度，有效类型：string slice，其他类型都将不能通过验证
//Alpha alpha字符，有效类型：string，其他类型都将不能通过验证
//Numeric 数字，有效类型：string，其他类型都将不能通过验证
//AlphaNumeric alpha 字符或数字，有效类型：string，其他类型都将不能通过验证
//Match(pattern string) 正则匹配，有效类型：string，其他类型都将被转成字符串再匹配(fmt.Sprintf(“%v”, obj).Match)
//AlphaDash alpha 字符或数字或横杠 -_，有效类型：string，其他类型都将不能通过验证
//Email 邮箱格式，有效类型：string，其他类型都将不能通过验证
//IP IP 格式，目前只支持 IPv4 格式验证，有效类型：string，其他类型都将不能通过验证
//Base64 base64 编码，有效类型：string，其他类型都将不能通过验证
//Mobile 手机号，有效类型：string，其他类型都将不能通过验证
//Tel 固定电话号，有效类型：string，其他类型都将不能通过验证
//Phone 手机号或固定电话号，有效类型：string，其他类型都将不能通过验证
//ZipCode 邮政编码，有效类型：string，其他类型都将不能通过验证
